export class agentsResponse {
     agentId: number;
     agenttypeID: string;
     airportID: string;
     agentName: string;
     agentAddress: string;
     countryID: string;
     stateID: string;
     cityID: string;
     CNIC: string;
     cnicImage: string;
     cnicExpiry: string;
     PhoneNo: string;
     cid:number;
     IATARegNo: string;
     emailAddress: string;
     faxNo: string;
     mobileNo: string;
     isDeleted: string;
     agentType: string;
     airportName: string;
     cityName: string;
     countryName: string;
     regionName: string;
}
export class requestAgent {
     agentId: number;
     agenttypeID: string;
     airportID: string;
     agentAddress: string;
     countryID: string;
     stateID: string;
     cityID: string;
     CNIC: string;
     cnicImage: string;
     cnicExpiry: string;
     PhoneNo: string;
     IATARegNo: string;
     emailAddress: string;
     agentName:string;
     faxNo: string;
     mobileNo: string;
     isNew:boolean;
     cid:number;
}
export class agentType{
     typeid:number;
     agentType:string;
}