export class requestAirLines {
    ALCode:number;
    ALName:string;
    Schedule:boolean;
    DOBy:boolean;
    DOAmount:number;
    Abbr:string;
    isNew:boolean;
}
export class responseAirLines {
    ALCode:number;
    ALName:string;
    Schedule:boolean;
    DOBy:boolean;
    DOAmount:number;
    Abbr:string;
}
