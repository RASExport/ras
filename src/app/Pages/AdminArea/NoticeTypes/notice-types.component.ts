import { Component, OnInit, ViewChildren,QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { responseNoticeTypes } from '../Models/noticeTypes';
import { ApiService } from '../../../Services/API/api.service';
import {requestGoods,responseGoods } from '../Models/Goods';
import { requestCity, requestStRegions, responseCity, responseCountries, responseRegions } from '../Models/cityState';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {GvarService} from '../../../Services/Globel/gvar.service'
import { consigneeResponse } from '../Models/consignee';
import { requestAirLines,responseAirLines } from '../Models/airLines';
import { ThemeService } from 'ng2-charts';

@Component({
  selector: 'app-notice-types',
  templateUrl: './notice-types.component.html',
  styleUrls: ['./notice-types.component.css']
})
export class NoticeTypesComponent implements OnInit {
  validForm:boolean=false;
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;

  shownewButton: boolean = true;
  showeditButton: boolean = false;
  showSaveButton: boolean = false;
  showCancelButton: boolean = false;
  showNoticeTypes: boolean = true;
  addnewNoticeType: boolean = false;

  responseGoods: responseGoods[];
  responseAirLines:responseAirLines[];
  noticeForm: FormGroup;
  responseNoticeTypes:responseNoticeTypes[];

  constructor(public API: ApiService, public GV: GvarService) {
    this.responseAirLines=[];
    this.responseNoticeTypes=[];
   }

  ngOnInit(): void {
    this.InitializeForm();
    this.getGoods();
    this.getAirLines();
    this.getNoticeTypes();
  }
  resetForm(value:any=undefined){
    this.noticeForm.reset(value);
   // (this as {submitted:boolean}.submitted=false);
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewNoticeType = true;
      this.showNoticeTypes = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.noticeForm.controls.isNew.setValue(true);
    }
    if (callfrm == "Cancel") {
      this.addnewNoticeType = false;
      this.showNoticeTypes = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.noticeForm.reset(this.noticeForm.value);
      this.resetForm();
      this.noticeForm.controls.isNew.setValue(false);
    }
    if (callfrm == "Edit") {
      this.addnewNoticeType = true;
      this.showNoticeTypes = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
      this.noticeForm.controls.isNew.setValue(false);
    }
  }
  getGoods(){
    this.API.getdata('/Setups/getNatofGoods').subscribe(c => {
      if (c != null) {
          this.responseGoods = c;
      }
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  InitializeForm(): any {
    this.noticeForm = new FormGroup({
      noticetypeID: new FormControl("", [Validators.required]),
      ALCode: new FormControl(),
      goodsId: new FormControl(),
      Destination: new FormControl(),
      mandatory: new FormControl(),
      isDeleted: new FormControl(),
      Nature: new FormControl(),
      ALName: new FormControl(),
      Schedule: new FormControl(),
      noticeType: new FormControl(),
      isNew:new FormControl(),


    });
  }
  getAirLines() {
    this.API.getdata('/Setups/getAirLines').subscribe(c => {
      if (c != null) {
          this.responseAirLines = c;
      }
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  destroyDT = (tableIndex, clearData): Promise<boolean> => {
    return new Promise((resolve) => {
      if(this.datatableElement)
      this.datatableElement.forEach((dtElement: DataTableDirective, index) => {

        if (index == tableIndex) {
          if (dtElement.dtInstance) {

            if (tableIndex == 0) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 1) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            } else if (tableIndex == 2) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 3) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 4) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }

          }
          else {
            resolve(true);
          }

        }
      });
    });
  };
  getNoticeTypes(){
    this.API.getdata('/Setups/getNoticeTypes').subscribe(c => {
      if (c != null) {
          this.responseNoticeTypes = c;
      }
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  validations() {
    if (this.noticeForm.controls.ALCode.value == "" || this.noticeForm.controls.ALCode.value == null) {
      Swal.fire({
        text: "Select Air Line.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.noticeForm.controls.goodsId.value == "" || this.noticeForm.controls.goodsId.value == null) {
      Swal.fire({
        text: "Select nature of goods.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.noticeForm.controls.noticeType.value == "" || this.noticeForm.controls.noticeType.value == null) {
      Swal.fire({
        text: "Select notice type.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    this.validForm = true;
  }
  saveNoticeType() {
    this.validations();
    if (this.validForm == true) {
      this.API.PostData('/Setups/saveNoticeTypes', this.noticeForm.value).subscribe(c => {
        if (c != null) {
          Swal.fire({
            text: "Notice Type saved successfully.",
            icon: 'success',
            confirmButtonText: 'OK'
          });
          this.showhide("Cancel");
          this.getNoticeTypes();
        }
      },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });

    }
  }
  editNoticeTypes(p){
    this.showhide("Edit");
    this.noticeForm.patchValue(p);
  }
}
