export class cargoClasses {
     CCode:string
     CName:string
     CAbbr:string
     CDescr:string
     DocCharges:string
     ConsolCharges:string
     SPHandling:string
}
export class finYearModel{
     yearid:number;
     finYer:string;
     startDate:Date;
     endDate:Date
}
export class requestModel{
     FromDate:string;
     ToDate:string;
     classType:string;
     finYer:number;
     selectedLocations:selectedLocations[];
     constructor(){
             this.selectedLocations=[];
     }
 }
 export class responseModel{
     constructor(){
         this.classWiseDetail=[];
         this.locationStatus=[];
     }
     classWiseDetail:classWiseDetail[];
     locationStatus:locationStatus[];
 }
 export class classWiseDetail {
     CName:string;
     GAmt:number;
     Revenue:string;
 }
 export class locationStatus {
     locationID:number;
     locationName:string;
     locStatus:string;
 }
 export class selectedLocations {
     locationID:number;
     locationName:string;
 }
 export class serverStatus{
     serverName:string;
     serverStatus:string;
 }