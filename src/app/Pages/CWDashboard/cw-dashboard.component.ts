import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, NumberValueAccessor } from '@angular/forms';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label} from 'ng2-charts';
import { ApiService } from '../../Services/API/api.service';
import { GvarService } from '../../Services/Globel/gvar.service'
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {serverStatus } from '../DashBoard/Model/DashBoardModel';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {Locations} from '../Login/Model/locations'
import {cargoClasses,requestModel,finYearModel,responseModel} from './Models/cargoClassMoedel'
import { throwIfEmpty } from 'rxjs/operators';

@Component({
  selector: 'app-cw-dashboard',
  templateUrl: './cw-dashboard.component.html',
  styleUrls: ['./cw-dashboard.component.css']
})
export class CwDashboardComponent implements OnInit {
  selectedYear:number=13;
  finYearModel:finYearModel[];
  cargoClasses:cargoClasses[];
  grandTotal:number=0;
  colors = [{ status: "Passed", color: "red" }, { status: "Approuved", color: "red" }, 
                { status: "warning", color: "green" }, { status: "Ignored", color: "yellow" }]
  serverStatus:serverStatus[];
  Locations:Locations[];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings = { 
    singleSelection: false,
    idField: 'locationID',
    textField: 'locationName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 8,
    allowSearchFilter: true
  };
  
  showBarChart: boolean = false;
  selectedReport:string="";
  selectedData:string;
  validForm: boolean = false;
  showpieChart:boolean=false;
  disableDate:boolean=true;
  complaintCount=[];
  chartSelectedValue: string = "bar";
  // PIE Chart 
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: {
        //fontFamily: '"Arvo", serif',
        fontSize: 20,
      }
    }
  };
  public pieChartLabels: Label[];
  public pieChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];


  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // En PieChart
  // Bar chart Start
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return "Rs " + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function (c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
          });
        }
      }
    },
    
  };
  public barChartLabels: Label[];
  public barChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsNaturewise: Label[];
  public barChartDataNaturewise: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsTrending: Label[];
  public barChartDataTrending: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];
  // End Bar chart
  DashBoardCWForm: FormGroup;
  valueFrom: Date;
  requestModel: requestModel;
  responseModel: responseModel;
  tempModel:responseModel;
  valueFromTime: Date = new Date();
  valueTo: Date;
  valueToTime: Date = new Date();
  fromDate = new Date();
  toDate = new Date();
  constructor(public API: ApiService, public GV: GvarService, private router: Router) {
    this.requestModel = new requestModel();
    this.responseModel = new responseModel();
    this.Locations=[];
    this.serverStatus=[];
    this.tempModel=new responseModel();
    this.cargoClasses=[];
    this.finYearModel=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getLocations();
    this.getClasses();
    this.getFinYear();
     this.DashBoardCWForm.controls['classType'].setValue("ALL", {onlySelf: true});
     this.DashBoardCWForm.controls['finYear'].setValue(this.selectedYear, {onlySelf: true});
  }
  InitializeForm(): any {
     this.DashBoardCWForm = new FormGroup({
      dateFrom: new FormControl(this.valueFrom, [Validators.required]),
      dateTo: new FormControl(this.valueTo, [Validators.required]),
      reportType: new FormControl('', [Validators.required]),
      finYear: new FormControl(null),
      classType: new FormControl(null)
    });
  }
  dateChangeEvent(args, callFrom) {
    if (args.value == null) {
      return;
    }
    if (callFrom == "FromDate") {

      this.fromDate = args.value;
    }
    if (callFrom == "ToDate") {
      this.toDate = args.value;
    }
  }
  changeLeaveTypeEvent(event) {

  }
  getData() {

    this.validations();
    if (this.validForm == true) {
      this.getGData();
      this.showBarChart = true;
      this.showpieChart=false;
      
    }
  }
  changeGrapth(event) {
    this.chartSelectedValue = event.target.value;
    if (this.chartSelectedValue == "bh") {
      this.barChartType = 'bar';
      this.showBarChart = true;
      this.showpieChart=false;
    }
    else if (this.chartSelectedValue == "lh") {
      this.showBarChart = true;
      this.showpieChart=false;
      this.barChartType = 'line';
    }
    else if (this.chartSelectedValue == "pi") {
      this.showBarChart = false;
      this.showpieChart=true;
      this.PieChartDataFetch();
     // this.barChartType = 'pie';
    }
  }
  getGData() {
    //this.getYear();
    this.requestModel.FromDate =  this.DashBoardCWForm.controls.dateFrom.value;
    this.requestModel.ToDate =  this.DashBoardCWForm.controls.dateTo.value;
    this.requestModel.classType =  this.DashBoardCWForm.controls.classType.value;
    this.requestModel.finYer=this.DashBoardCWForm.controls.finYear.value;
    this.API.PostData('/CWCHMS/getCWData', this.requestModel).subscribe(c => {
      if (c != null) {
        this.responseModel = c;
        this.tempModel=c;
        this.grandTotal=0;
        this.responseModel.classWiseDetail.forEach(e => {
         this.grandTotal=this.grandTotal+e.GAmt;
        });
       this.responseModel = c;
        this.complaintCount = [];
        this.complaintCount = this.responseModel.classWiseDetail.map((item) => {
          return item.GAmt;
        });
        this.barChartDataNaturewise = [{ data: this.complaintCount, backgroundColor: '#2196f3',hoverBackgroundColor:'#28196D',fill:false }];
        var complaintDept = [];
        complaintDept = this.responseModel.classWiseDetail.map((item) => {
            return item.CName;
        });
        this.barChartLabelsNaturewise = complaintDept;
      }
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      })
  }
  PieChartDataFetch() {
        this.complaintCount = this.responseModel.classWiseDetail.map((item) => {
          return item.GAmt;
        });
        var complaintCount = [];
        this.pieChartData = [{ data: this.complaintCount}];
        var complaintDept = [];
        complaintDept = this.responseModel.classWiseDetail.map((item) => {
            return item.CName;
        });
        this.pieChartLabels = complaintDept;
  }
  validations() {

    if (this.requestModel.selectedLocations === undefined || this.requestModel.selectedLocations.length == 0) {
      Swal.fire({
        text: "Please select location.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if ( this.DashBoardCWForm.controls.reportType.value === undefined ||  this.DashBoardCWForm.controls.reportType.value == "") {
      Swal.fire({
        text: "Please select report type.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if ( this.DashBoardCWForm.controls.finYear.value === undefined ||  this.DashBoardCWForm.controls.finYear.value == "") {
      Swal.fire({
        text: "Please select financial year.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if ( this.DashBoardCWForm.controls.classType.value == "" ||  this.DashBoardCWForm.controls.classType.value == null) {
      Swal.fire({
        text: "Please select class Type.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if ( this.DashBoardCWForm.controls.dateFrom.value == "" ||  this.DashBoardCWForm.controls.dateFrom.value == null) {
      Swal.fire({
        text: "Please select from date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
      if ( this.DashBoardCWForm.controls.dateTo.value == "" ||  this.DashBoardCWForm.controls.dateTo.value == null) {
        Swal.fire({
          text: "Please select to date.",
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.validForm = false;
        return;
      }
    this.validForm = true;
  }
  changeReportType(event){
    this.chartSelectedValue = event.target.value;
    if(this.chartSelectedValue=="FY"){
      //this.valueFrom=new Date("07/01/2021");
       this.DashBoardCWForm.controls.dateFrom.setValue(new Date("07/01/2020"));
       this.DashBoardCWForm.controls.dateTo.setValue(new Date("06/30/2021"));
      this.disableDate=true;
    }
    if(this.chartSelectedValue=="CY"){
       this.DashBoardCWForm.controls.dateFrom.setValue(new Date("01/01/2021"));
       this.DashBoardCWForm.controls.dateTo.setValue(new Date("12/31/2021"));
      this.disableDate=false;
     
    }
  }
  changeDataType(event){
    this.selectedData=event.target.value;
  }
  onItemSelect(item: any) {
    this.requestModel.selectedLocations.push(item);
    console.log(item);
  }
  onSelectAll(items: any) {
   // debugger
    this.requestModel.selectedLocations=items
  }
  deSelectAll(items: any) {
    this.requestModel.selectedLocations=[];
  }
  ondeSelect(items: any ){
    this.requestModel.selectedLocations.splice(this.requestModel.selectedLocations.findIndex(ele => ele.locationID == items.locationID), 1);
  }
  getLocations(){
    this.API.getdata('/Locations/getLocations').subscribe(data=>{
      if(data!=null){
        this.dropdownList=data;
      }
    })
  }
  getClasses(){
    this.API.getdata('/CWCHMS/getCargoClasses').subscribe(data=>{
      if(data!=null){
        this.cargoClasses=data;
      }
    })
  }
  getFinYear(){
    this.API.getdata('/CWCHMS/getfinYEar').subscribe(data=>{
      if(data!=null){
        this.finYearModel=data;
      }
    })
  }
  getTheColor(status) {
    return this.colors.filter(item => item.status === status)[0].color 
    // could be better written, but you get the idea
}
}

