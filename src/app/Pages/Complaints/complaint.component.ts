import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import {complaintTypes,ComplaintCounter,pendingComplaints,complaints} from './Models/complaints'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../../Services/API/api.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import {GvarService} from '../../Services/Globel/gvar.service'
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.component.html',
  styleUrls: ['./complaint.component.css']
})
export class ComplaintComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild("fileUpload") fileUpload: ElementRef; files = [];
  fileName: string = "Choose file...";
  complaintTypes:complaintTypes[];
  ComplaintCounter:ComplaintCounter[];
  pendingComplaints:pendingComplaints[];
  complaintModel:complaints;
  ComplaintForm: FormGroup;
  constructor(public API: ApiService, public GV:GvarService,private router: Router) { 
    this.complaintTypes=[];
    this.ComplaintCounter=[];
    this.pendingComplaints=[];
    this.complaintModel=new complaints;
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getComplaintTypes();
  }
  InitializeForm(): any {
    this.ComplaintForm = new FormGroup({
      complaintType: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
    });
  }
  getComplaintTypes(){
    this.API.getdata('/Complaints/getComplaintTypes').subscribe(data=>{
      if(data!=null){
        this.complaintTypes=data;
      }
    })
  }
  changeComplaintTypeEvent(event){
  }
  submitComplaint (){
    if(this.ComplaintForm.valid){
      this.complaintModel.descripton= this.ComplaintForm.get('description').value;
      this.complaintModel.typeID=this.ComplaintForm.get('complaintType').value;
      this.complaintModel.locationID=this.GV.locationID;
      //debugger
      this.API.PostData('/Complaints/saveComplaint', this.complaintModel).subscribe(c => {
        if (c != null) {
          Swal.fire({
            text: 'Complaint has been generated successfully. Your complaint number is " ' + c +' "',
            icon: 'success',
            confirmButtonText: 'OK'
          });
          this.router.navigate(['/Dashboard']);
        }
      },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        })
      }
    }
  onClick() {
    const fileUpload = this.fileUpload.nativeElement; fileUpload.onchange = () => {
      this.files=[];
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.fileName = this.files[0].data.name;
    };
   
    fileUpload.click();
  }
}
