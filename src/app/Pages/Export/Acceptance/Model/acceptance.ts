export class requestAcceptance {
    station: string;
    acceptanceID: number
    ALCode: number
    ALName: string
    Destination: string
    AWBType: string
    FurShipment: string
    DNR: boolean
    Occurance: boolean
    OvrShipment: boolean
    holdShipment: boolean
    otherAirline: boolean
    otherAirLineName: string
    empID: string
    empName: string
    ArrivalDate: string;
    ArrivalTime: string
    otherAirlineCode: string
    docNo: number
    docDate: string
    HandedDate: string
    HandedTime: Date
    Status: string
    Remarks: string
    isNew: boolean
}
export class acceptanceModel {
    generalRequest: requestAcceptance;
    awbDetailRequest: AWBDetailRequest;
    constructor() {
        this.generalRequest = new requestAcceptance();
        this.awbDetailRequest = new AWBDetailRequest();
    }
}
export class responseModel {
    isSaved: boolean;
    responseAcceptance: responseAcceptance;
    responseModel() {
        this.responseAcceptance = new responseAcceptance();
    }

}
export class responseAcceptance {
    station: string;
    acceptanceID: number
    ALCode: number
    ALName: string
    Destination: string
    AWBType: string
    FurShipment: string
    DNR: boolean
    Occurance: boolean
    OvrShipment: boolean
    holdShipment: boolean
    otherAirline: boolean
    otherAirLineName: string
    empID: string
    empName: string
    ArrivalTime: string
    otherAirlineCode: string
    docNo: number
    docDate: string
    HandedDate: string
    HandedTime: string
    Status: string
    Remarks: string
}
export class employeeModel {
    empID: string;
    employeeName: string;
}
export class AWBDetailRequest {
    acceptanceID: string;
    AWBNo: string;
    flightNo: string;
    aricraftRegNo: string;
    departureDate: string;
    departureTime: string;
    Destination: string;
    goodsId: string;
    Pieces: string;
    grossWeight: string;
    dimensionalWeight: string;
    comid: string;
    cuttTime: string;
    agentId: string;
    consolidatorID: string;
}
export class requestWeight {
    weightDetailID: string;
    acceptanceID: string;
    AWBNo: string;
    empID: string;
    vehicleID: string;
    vehNumer: string;
    driverName: string;
    driverCNIC: string;
    firstWt: string;
    firstTime: string;
    firstDate: string;
    secondWt: string;
    secondTime: string;
    secondDate: string;
    AWBWt: string;
    netWt: string;
    remarks: string;
}
export class weightResponseModel {
    weightResponse: requestWeight;
    weightDetailResponse: weightDetailResponse[];
    constructor() {
        this.weightResponse = new requestWeight();
        this.weightDetailResponse = [];

    }
}
class weightDetailResponse {
    weightID: string;
    acceptanceID: string;
    airportID: string;
    AWBNo: string;
    empID: string;
    isDeleted: string;
    weightDetailID: string;
    vehicleID: string;
    vehNumer: string;
    driverName: string;
    driverCNIC: string;
    firstWt: string;
    firstTime: string;
    firstDate: string;
    secondWt: string;
    secondTime: string;
    secondDate: string;
    AWBWt: string;
    remarks: string;
    netWt: string;
    isDeleted_Detail: string;
    vehicleType: string;
}
export class getWeight {
    weightID: string;
    acceptanceID: string;
}
export class attachmentResponse {
    attachmentID: number;
    module: string;
    fileName: string;
    ContentType: string;
    fileData: Blob;
    acceptanceID: number;
}
export class noticeTypesRequest {
    ALCode: number;
    goodsId: number;
}
export class InquiryResponse {
    noticetypeID: string;
    ALCode: string;
    goodsId: string;
    mandatory: boolean;
    Nature: string;
    ALName: string;
    Schedule: string;
    destination: string;
    noticeType: string;
    revieved: boolean;
    pending: boolean;
    na: boolean;
}
export class dimWeightResponse {
    dimWeightID: string;
    acceptanceID: string;
    AWBNo: string;
    goodsid: string;
    pieces: string;
    length: string;
    width: string;
    height: string;
    sizeinCM: string;
    totalWeight: string;
    remarks: string;
    Nature: string;
}
export class HouseAWB {
    HNo: string;
    HAWBNo: string;
    acceptanceID: string;
    flightID: string;
    AWBNo: string;
    grossWeight: string;
    chargeableWeight: string;
    width: string;
    height: string;
    length: string;
    pieces: string;
    dimensionalWeight: string;
    comid: string;
    goodsid: string;
    shipperId: string;
    cid: string;
    flightNo: string;
    ALCode: string;
    regNo: string;
    depDate: string;
    Destination: string;
    depTime: string;
    Nature: string;
}