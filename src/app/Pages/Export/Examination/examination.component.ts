import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { AWBDetail, responseExamination } from './ExaminationModel';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ApiService } from '../../../Services/API/api.service';
import { GvarService } from '../../../Services/Globel/gvar.service'

@Component({
  selector: 'app-examination',
  templateUrl: './examination.component.html',
  styleUrls: ['./examination.component.css']
})
export class ExaminationComponent implements OnInit {
  responseExamination: responseExamination[];
  AWBNo: string;
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  AWBDetail: AWBDetail[];
  examinationForm: FormGroup;
  AWBForm: FormGroup;
  constructor(public API: ApiService, public GV: GvarService) {
    this.AWBDetail = [];
    this.InitializeForm();
    this.InitializeFormAWB();
    this.responseExamination = [];
  }

  ngOnInit(): void {
  }
  SaveDetail() {
    this.examinationForm.controls.isNew.setValue(false);
    this.API.PostData('/Examination/saveExamination', this.examinationForm.value).subscribe(c => {
      if (c != null) {
        Swal.fire({
          text: "Examination data has been updated successfully.",
          icon: 'success',
          confirmButtonText: 'OK'
        });
      }
      this.getExamination();
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.examinationForm.reset(true);
      });
  }
  getAWBDetail() {
    this.API.getdata('/Acceptance/getAWBDetail?AWBNo=' + this.AWBNo).subscribe(c => {
      if (c != null) {
        this.AWBForm.patchValue(c);
        this.getExamination();
        // this.destroyDT(0, false).then(destroyed => {
        //   this.AWBDetail = c;
        //   this.dtTrigger.next();
        // });
      }
    },
      error => {

        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.examinationForm.reset(true);
      });
  }
  getExamination() {
    this.API.getdata('/Examination/getExamination?AWBNo=' + this.AWBNo).subscribe(c => {
      if (c != null) {
        this.AWBForm.patchValue(c);
        this.destroyDT(0, false).then(destroyed => {
          this.responseExamination = c;
          this.dtTrigger.next();
        });
      }
    },
      error => {

        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.examinationForm.reset(true);
      });
  }
  InitializeFormAWB(): any {
    this.AWBForm = new FormGroup({
      acceptanceID: new FormControl(""),
      ALCode: new FormControl(""),
      AWBType: new FormControl(""),
      comm_description: new FormControl(""),
      comid: new FormControl(""),
      AWBNo: new FormControl(""),
      flightID: new FormControl(""),
      flightNo: new FormControl(""),
      depDate: new FormControl(""),
      depTime: new FormControl(""),
      Destination: new FormControl(""),
      isDepartured: new FormControl(""),
      airportID: new FormControl(""),
      Pieces: new FormControl(""),
      grossWeight: new FormControl(""),
      dimensionalWeight: new FormControl(""),
      regNo: new FormControl(""),
      Nature: new FormControl(""),
      goodsId: new FormControl(""),
      ALName: new FormControl(""),
    });
  }
  InitializeForm(): any {
    this.examinationForm = new FormGroup({
      examinationID: new FormControl(""),
      AWBNo: new FormControl(""),
      occurance: new FormControl(""),
      description: new FormControl(""),
      pieces: new FormControl(""),
      examinationType: new FormControl(""),
      completed: new FormControl(""),
      pending: new FormControl(""),
      exempt: new FormControl(""),
      anfCustomExemptionNo: new FormControl(""),
      remarks: new FormControl(""),
      isNew: new FormControl(""),
    });
  }
  destroyDT = (tableIndex, clearData): Promise<boolean> => {
    return new Promise((resolve) => {
      if (this.datatableElement)
        this.datatableElement.forEach((dtElement: DataTableDirective, index) => {

          if (index == tableIndex) {
            if (dtElement.dtInstance) {

              if (tableIndex == 0) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              }
              else if (tableIndex == 1) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              } else if (tableIndex == 2) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              }
              else if (tableIndex == 3) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });

              }
              else if (tableIndex == 4) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              }
            }
            else {
              resolve(true);
            }
          }
        });
    });
  };
  editExamination(p) {
    this.examinationForm.patchValue(p);
  }
}
