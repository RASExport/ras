export class requestFlight {
    flightID: string;
    flightNo: string;
    ALCode: string;
    regNo: string;
    depDate: string;
    depTime: string;
    Destination: string;
    isDepartured: string;
    isNew: boolean;
}
export class responseFlight {
    flightID: string;
    flightNo: string;
    ALCode: string;
    regNo: string;
    depDate: string;
    depTime: string;
    Destination: string;
    isDepartured: string;
    airportID: string;
    airportName: string;
    ALName: string;
}