import { Component, OnInit, ViewChildren,QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { requestFlight,responseFlight} from '../Flights/Model/flightModel';
import { ApiService } from '../../../Services/API/api.service';
import { responseAirLines } from '../../AdminArea/Models/airLines';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {GvarService} from '../../../Services/Globel/gvar.service'
import { ThemeService } from 'ng2-charts';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit{
  public selectionMode = 'single';
  public $rowEditEnter = false;
  public $cellEditEnter = false;
  public $cellEdit = false;
  public $rowEdit = false;
  public data;
  public departureDate: Date = new Date();
  responseAirLines:responseAirLines[];
  responseFlight: responseFlight[];
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;

  validForm: boolean = false;
  requestFlight: requestFlight;
  flightForm: FormGroup;
  shownewButton: boolean = true;
  showeditButton: boolean = false;
  showSaveButton: boolean = false;
  showCancelButton: boolean = false;
  showFlights: boolean = true;
  addnewFlight: boolean = false;
  constructor(public API: ApiService,public GV:GvarService) {
    this.responseFlight =[];
    this.requestFlight=new requestFlight();
    this.responseAirLines=[];
  }
  InitializeForm(): any {
    this.flightForm = new FormGroup({
      flightID: new FormControl("", [Validators.required]),
      flightNo: new FormControl(""),
      ALCode: new FormControl("", [Validators.required]),
      regNo: new FormControl("", [Validators.required]),
      depDate: new FormControl(this.departureDate, [Validators.required]),
      depTime: new FormControl("", [Validators.required]),
      Destination:new FormControl("", [Validators.required]),
      isDepartured: new FormControl("", [Validators.required]),
    });
  }
  ngOnInit(): void {
    this.InitializeForm();
    this.getFlights();
    this.getAirLines();

  }
  getFlights(){
    this.API.getdata('/Flights/getFlights').subscribe(c => {
      if (c != null) {
          this.responseFlight = c;
      }
    },
      error => {
       
        Swal.fire({
          text: error,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
    showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewFlight = true;
      this.showFlights = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.requestFlight.isNew=true;
    }
    if (callfrm == "Cancel") {
      this.addnewFlight = false;
      this.showFlights = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.flightForm.reset(this.flightForm.value);
      this.resetForm();
      this.requestFlight.isNew=false;
    }
    if (callfrm == "Edit") {
      this.addnewFlight = true;
      this.showFlights = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
      this.requestFlight.isNew=false;
    }
  }
  resetForm(value:any=undefined){
    this.flightForm.reset(value);
  }
  saveFlights() {
    this.validations();
    if (this.validForm == true) {
      this.requestFlight.flightID=this.flightForm.controls.flightID.value;
      this.requestFlight.flightNo = this.flightForm.controls.flightNo.value;
      this.requestFlight.ALCode = this.flightForm.controls.ALCode.value;
      this.requestFlight.regNo = this.flightForm.controls.regNo.value;
      this.requestFlight.depDate = this.flightForm.controls.depDate.value;
      this.requestFlight.depTime=this.flightForm.controls.depTime.value;
      this.requestFlight.Destination=this.flightForm.controls.Destination.value;
      this.requestFlight.isDepartured=this.flightForm.controls.isDepartured.value;
      this.API.PostData('/Flights/saveFlights', this.requestFlight).subscribe(c => {
        if (c != null) {
          Swal.fire({
            text: "Flight added successfully.",
            icon: 'success',
            confirmButtonText: 'OK'
          });
          this.showhide("Cancel");
          this.getFlights();
        }
        });
      }
  }
  validations() {
    if (this.flightForm.controls.ALCode.value == "" || this.flightForm.controls.ALCode.value == null) {
      Swal.fire({
        text: "Select AirLine.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.flightForm.controls.regNo.value == "" || this.flightForm.controls.regNo.value == null) {
      Swal.fire({
        text: "Enter Aircraft Reg. No.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.flightForm.controls.depDate.value == "" || this.flightForm.controls.depDate.value == null) {
      Swal.fire({
        text: "Enter departure date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.flightForm.controls.depTime.value == "" || this.flightForm.controls.depTime.value == null) {
      Swal.fire({
        text: "Enter departure time.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.flightForm.controls.Destination.value == "" || this.flightForm.controls.Destination.value == null) {
      Swal.fire({
        text: "Enter destination.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
        this.validForm = true;
  }
  editAirLines(p,i){
    this.showhide("Edit");
    this.flightForm.setValue({
      flightID:p.flightID,
      flightNo : p.flightNo,
      ALCode : p.ALCode,
      regNo : p.regNo,
      depDate : p.depDate,
      depTime:p.depTime,
      Destination : p.Destination,
      isDepartured : p.isDepartured,
    })
  }
  getAirLines() {
    this.API.getdata('/Setups/getAirLines').subscribe(c => {
      if (c != null) {
        this.responseAirLines = c;
      }
    }, 
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  public rowEditEnter(evt) {
    var p=evt.newValue
    this.editAirLines(p,1);
}
public cellEditEnter(evt) {
  var p=evt.newValue
    evt.cancel = this.$cellEditEnter;
}
public cellEdit(evt) {
    evt.cancel = this.$cellEdit;
}
public cellEditDone() {
}
public cellEditExit() {
}
public rowEdit(evt) {
    evt.cancel = this.$rowEdit;
}
public rowEditDone(evt) {
  var p=evt.newValue
  this.flightForm.setValue({
    flightID:p.flightID,
    flightNo : p.flightNo,
    ALCode : p.ALCode,
    regNo : p.regNo,
    depDate : p.depDate,
    depTime:p.depTime,
    Destination : p.Destination,
    isDepartured : p.isDepartured,
  })
  this.saveFlights();
}
public rowEditExit() {
}
}
