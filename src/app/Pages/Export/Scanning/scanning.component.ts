import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { FormGroup, FormControl, Validators, FormControlDirective } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { AWBDetail, scanningResponse } from './scanningModel';
import { Subject } from 'rxjs';
import { employeeModel } from '../../Export/Acceptance/Model/acceptance'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ApiService } from '../../../Services/API/api.service';
import { GvarService } from '../../../Services/Globel/gvar.service'

@Component({
  selector: 'app-scanning',
  templateUrl: './scanning.component.html',
  styleUrls: ['./scanning.component.css']
})
export class ScanningComponent implements OnInit {
  validForm: boolean = false;
  scanningPerson: employeeModel;
  securityPerson: employeeModel;

  scanningResponse: scanningResponse[];
  AWBNo: string;
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  AWBDetail: AWBDetail[];
  scanningForm: FormGroup;
  AWBForm: FormGroup;
  constructor(public API: ApiService, public GV: GvarService) {
    this.AWBDetail = [];
    this.InitializeForm();
    this.InitializeFormAWB();
    this.scanningResponse = [];
    this.scanningPerson = new employeeModel();
    this.securityPerson = new employeeModel();
  }

  ngOnInit(): void {
    this.scanningForm.controls.isNew.setValue(true);
  }
  SaveDetail() {
    this.Validations();
    if(this.validForm==false) {
      return
    }
    
    this.scanningForm.controls.AWBNo.setValue(this.AWBForm.controls.AWBNo.value)
    this.scanningForm.controls.acceptanceID.setValue(this.AWBForm.controls.acceptanceID.value)
    this.API.PostData('/Scanning/saveScanning', this.scanningForm.value).subscribe(c => {
      if (c != null) {
        Swal.fire({
          text: "Scanning data has been updated successfully.",
          icon: 'success',
          confirmButtonText: 'OK'
        });
        this.scanningForm.controls.scanningID.setValue(c.scanningID);
      }
      this.scanningForm.controls.isNew.setValue(false);
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  getAWBDetail() {
    this.API.getdata('/Acceptance/getAWBDetail?AWBNo=' + this.AWBNo).subscribe(c => {
      if (c != null) {
        this.AWBForm.patchValue(c);
        this.scanningForm.reset(true);
        this.scanningForm.controls.isNew.setValue(true);
        

        // this.destroyDT(0, false).then(destroyed => {
        //   this.AWBDetail = c;
        //   this.dtTrigger.next();
        // });
      }
    },
      error => {

        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.scanningForm.reset(true);
      });
  }
  
  InitializeFormAWB(): any {
    this.AWBForm = new FormGroup({
      acceptanceID: new FormControl(""),
      ALCode: new FormControl(""),
      AWBType: new FormControl(""),
      comm_description: new FormControl(""),
      comid: new FormControl(""),
      AWBNo: new FormControl(""),
      flightID: new FormControl(""),
      flightNo: new FormControl(""),
      depDate: new FormControl(""),
      depTime: new FormControl(""),
      Destination: new FormControl(""),

      isDepartured: new FormControl(""),
      airportID: new FormControl(""),
      Pieces: new FormControl(""),
      grossWeight: new FormControl(""),
      dimensionalWeight: new FormControl(""),
      regNo: new FormControl(""),
      Nature: new FormControl(""),
      goodsId: new FormControl(""),
      ALName: new FormControl(""),
    });
  }
  InitializeForm(): any {
    this.scanningForm = new FormGroup({
      scanningID: new FormControl(""),
      station: new FormControl(""),
      AWBNo: new FormControl(""),
      HAWBNO: new FormControl(""),
      scanningType: new FormControl(""),
      securityPersonalID: new FormControl(""),
      scaningPersonalID: new FormControl(""),
      acceptanceID: new FormControl(""),
      scannedStatus: new FormControl(""),
      scanningTime: new FormControl(""),
      scanningDate: new FormControl(""),
      scanPName: new FormControl(""),
      occurranceDetail: new FormControl(""),
      frustratedShipment: new FormControl(""),
      occurance: new FormControl(""),
      oversizeShipments: new FormControl(""),
      physicallyChecked: new FormControl(""),
      remarks: new FormControl(""),
      ETD: new FormControl(""),
      EDD: new FormControl(""),
      isNew: new FormControl(""),
      spName: new FormControl(""),
    });
  }
  destroyDT = (tableIndex, clearData): Promise<boolean> => {
    return new Promise((resolve) => {
      if (this.datatableElement)
        this.datatableElement.forEach((dtElement: DataTableDirective, index) => {

          if (index == tableIndex) {
            if (dtElement.dtInstance) {

              if (tableIndex == 0) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              }
              else if (tableIndex == 1) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              } else if (tableIndex == 2) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              }
              else if (tableIndex == 3) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });

              }
              else if (tableIndex == 4) {
                dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  if (clearData) {
                    dtInstance.clear();
                  }
                  dtInstance.destroy();
                  resolve(true);
                });
              }
            }
            else {
              resolve(true);
            }
          }
        });
    });
  };

  getSecurityPerson() {
    if (this.scanningForm.controls.securityPersonalID.value != "" && this.scanningForm.controls.securityPersonalID.value != null) {
      this.API.getdata('/Generic/getEmpDetail?empID=' + this.scanningForm.controls.securityPersonalID.value).subscribe(c => {
        if (c != null) {
          this.securityPerson = c;
          this.scanningForm.controls.spName.setValue(this.securityPerson.employeeName);
        }
      },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getScanningPerson() {
    if (this.scanningForm.controls.scaningPersonalID.value != "" && this.scanningForm.controls.scaningPersonalID.value != null) {
      this.API.getdata('/Generic/getEmpDetail?empID=' + this.scanningForm.controls.scaningPersonalID.value).subscribe(c => {
        if (c != null) {
          this.scanningPerson = c;
          this.scanningForm.controls.scanPName.setValue(this.scanningPerson.employeeName);
        }
      },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  Validations() {
    if (this.AWBForm.controls.AWBNo.value == "" || this.AWBForm.controls.AWBNo.value == null) {
      Swal.fire({
        text: "Select AWB Detail first.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.scanningForm.controls.securityPersonalID.value == "" || this.scanningForm.controls.securityPersonalID.value == null) {
      Swal.fire({
        text: "Select security person.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.scanningForm.controls.scaningPersonalID.value == "" || this.scanningForm.controls.scaningPersonalID.value == null) {
      Swal.fire({
        text: "Select scanning person.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.scanningForm.controls.scanningType.value == "" || this.scanningForm.controls.scanningType.value == null) {
      Swal.fire({
        text: "Select scanning type.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.scanningForm.controls.scannedStatus.value == "" || this.scanningForm.controls.scannedStatus.value == null) {
      Swal.fire({
        text: "Select scanning status.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.scanningForm.controls.scanningTime.value == "" || this.scanningForm.controls.scanningTime.value == null) {
      Swal.fire({
        text: "Enter scanning time.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.scanningForm.controls.scanningDate.value == "" || this.scanningForm.controls.scanningDate.value == null) {
      Swal.fire({
        text: "Enter scanning date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    this.validForm = true;
  }
}
