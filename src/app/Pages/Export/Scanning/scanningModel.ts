export class scanningResponse {
    scanningID: string;
    station: string;
    AWBNO: string;
    HAWBNO: string;
    scanningType: string;
    securityPersonalID: string;
    scaningPersonalID: string;
    acceptanceID: string;
    scannedStatus: string;
    scanningTime: string;
    scanningDate: string;
    occurrance: string;
    frustratedShipment: string;
    occurance: string;
    oversizeShipments: string;
    physicallyChecked: string;
    remarks: string;
    ETD: string;
    EDD: string;
    isNew: string;
}
export class AWBDetail {
    acceptanceID: string;
    ALCode: string;
    AWBType: string;
    comm_description: string;
    comid: string;
    AWBNo: string;
    flightID: string;
    flightNo: string;
    depDate: string;
    depTime: string;
    Destination: string;
    isDepartured: string;
    airportID: string;
    Pieces: string;
    grossWeight: string;
    dimensionalWeight: string;
    regNo: string;
    Nature: string;
    goodsId: string;
    ALName: string;
}