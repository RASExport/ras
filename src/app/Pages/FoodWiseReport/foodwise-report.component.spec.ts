import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodwiseReportComponent } from './foodwise-report.component';

describe('FoodwiseReportComponent', () => {
  let component: FoodwiseReportComponent;
  let fixture: ComponentFixture<FoodwiseReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodwiseReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodwiseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
