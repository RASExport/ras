import { Component, OnInit,ViewChildren,QueryList } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {foodwiseRequestModel,foodwiseResponseModel} from './Models/foodwiseModel'
import { ApiService } from '../../Services/API/api.service';
import { GvarService } from '../../Services/Globel/gvar.service'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label, Color ,SingleDataSet} from 'ng2-charts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-foodwise-report',
  templateUrl: './foodwise-report.component.html',
  styleUrls: ['./foodwise-report.component.css']
})
export class FoodwiseReportComponent implements OnInit {

  grandTotal:number=0;
  colors = [{ status: "Passed", color: "red" }, { status: "Approuved", color: "red" }, 
                { status: "warning", color: "green" }, { status: "Ignored", color: "yellow" }]
  showBarChart: boolean = false;
  selectedReport:string="";
  selectedData:string;
  validForm: boolean = false;
  showpieChart:boolean=false;
  disableDate:boolean=true;
  complaintCount=[];
  chartSelectedValue: string = "bar";
  // PIE Chart 
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: {
        //fontFamily: '"Arvo", serif',
        fontSize: 20,
      }
    }
  };
  public pieChartLabels: Label[];
  public pieChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];


  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // En PieChart
  // Bar chart Start
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
    },
    scales: {
      xAxes: [{
        ticks: {}
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          stepSize: 5000,
        }
      }]
    }
  };
  public barChartLabels: Label[];
  public barChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsNaturewise: Label[];
  public barChartDataNaturewise: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsTrending: Label[];
  public barChartDataTrending: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];




  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;

  dtOptions0: any = {};
  dtTrigger0: Subject<any> = new Subject();

  foodwiseResponseModel:foodwiseResponseModel[];
  foodwiseRequestModel:foodwiseRequestModel;
  dateFormat:string="dd-MMMM-yyyy";
  hourlyFrom: FormGroup;
  valueFrom: Date;
  valueTo: Date;
  constructor(public API: ApiService, public GV: GvarService,private router: Router) { 
    this.foodwiseRequestModel=new foodwiseRequestModel();
    this.foodwiseResponseModel=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
  }
  InitializeForm(): any {
    this.hourlyFrom = new FormGroup({
      dateFrom: new FormControl(this.valueFrom, [Validators.required]),
      dateTo: new FormControl(this.valueTo, [Validators.required]),
    });
  }
  getData(){
    this.validations();
    if (this.validForm == true) {
    this.foodwiseRequestModel.mFromDate=this.hourlyFrom.controls.dateFrom.value.toDateString();
    this.foodwiseRequestModel.mToDate=this.hourlyFrom.controls.dateTo.value.toDateString();
    this.API.PostData('/POS/getfoodWiseReport', this.foodwiseRequestModel).subscribe(c => {
        this.foodwiseResponseModel=c;
        this.dtTrigger0.next();
        this.showBarChart=true;
        this.getBarChartHorizental();
      }), error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.router.navigateByUrl('/login');
      }
      
  }

  }
  validations(){
    if (this.hourlyFrom.controls.dateFrom.value == "" || this.hourlyFrom.controls.dateFrom.value == null) {
      Swal.fire({
        text: "Please select from date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.hourlyFrom.controls.dateTo.value == "" || this.hourlyFrom.controls.dateTo.value == null) {
      Swal.fire({
        text: "Please select to date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    this.validForm=true;
  }
  getBarChartHorizental() {
    this.grandTotal=0;
    this.foodwiseResponseModel.forEach(e => {
      this.grandTotal=this.grandTotal+e.SAmt;
    });
    this.complaintCount = this.foodwiseResponseModel.map((item) => {
      return item.SAmt;
    });
    this.barChartDataNaturewise = [{ data: this.complaintCount, backgroundColor: '#2196f3',hoverBackgroundColor:'#28196D',fill:false }];
    var complaintDept = [];
    complaintDept = this.foodwiseResponseModel.map((item) => {
        return item.Mname;
    });
    this.barChartLabelsNaturewise = complaintDept;
  }
}
