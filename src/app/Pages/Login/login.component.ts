import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserModel } from './Model/Users'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GvarService } from '../../Services/Globel/gvar.service';
import { ApiService } from '../../Services/API/api.service'
import { Locations } from './Model/locations'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthService } from '../../Services/Auth/auth.service'
import { templateJitUrl } from '@angular/compiler';
declare var jQuery: any;
const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  validForm: boolean = false;
  isReadOnly = true;
  loginForm: FormGroup;
  loginViewModel: UserModel
  clicked = false;
  Locations: Locations[];
  InvalidLogin: boolean;
  errorMessage: string;
  Roles: any = [];
  returnUrl: string;
  constructor(private _el: ElementRef,
    public API: ApiService,
    public route: Router,
    private activatedRoute: ActivatedRoute,
    private GV: GvarService,
    private authService: AuthService,

  ) {
    this.loginViewModel = new UserModel();
    this.Locations = [];
  }
  ngOnInit() {

    setTimeout(() => {
      this.isReadOnly = false;
    }, 2000);
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params['returnUrl'] || '/';
    });
    //this.authService.Logout();
    this.InitializeForm();
    this.getLocations();
  }
  InitializeForm(): any {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      //location: new FormControl('Head Office', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.maxLength(100), Validators.minLength(2)]),
    });
  }
  onLoginClick() {
    this.validations();
    if (this.validForm == true) {
      if (this.loginForm.valid) {
        this.GV.locationID = 1;
        this.clicked = true;
        this.loginForm.disable({ emitEvent: true });
        this.API.LoginUser('/token', { username: this.loginForm.get('username').value, password: this.loginForm.get('password').value, location: 1 }).subscribe(
          data => {
            debugger
            localStorage.setItem('access_token', data.access_token);
            localStorage.setItem('userRoles', data.Roles);
            localStorage.setItem('userName', data.Roles);
            this.route.navigate([this.returnUrl]);
          },
          error => {
            this.InvalidLogin = true;
            this.clicked = false;
            this.loginForm.enable({ emitEvent: true });
            if (error.error.error_description != undefined) {
              Swal.fire('CK', error.error.error_description, 'error')
            }
            else {
              Swal.fire('CK', 'Network Error.', 'error')
            }

          });
      }
    }
  }
  getLocations() {
    this.API.getdata('/Generic/getLocations').subscribe(data => {
      if (data != null) {
        this.Locations = data;
      }
    })
  }
  changeLocations(event) {

  }
  validations() {
    // if (this.loginForm.controls.location.value == "" || this.loginForm.controls.location.value == null) {
    //   Swal.fire({
    //     text: "Please select location.",
    //     icon: 'error',
    //     confirmButtonText: 'OK'
    //   });
    //   this.validForm = false;
    //   return;
    // }
    if (this.loginForm.controls.username.value == "" || this.loginForm.controls.username.value == null) {
      Swal.fire({
        text: "Please enter username.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.loginForm.controls.password.value == "" || this.loginForm.controls.password.value == null) {
      Swal.fire({
        text: "Please enter password.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    this.validForm=true;
  }
}

