
import { Component, Inject, ViewChild, OnInit, Output } from '@angular/core';
import {POPOUT_MODAL_DATA, PopoutData} from '../../Pages/Shared/Service/popout.tokens';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from '../../Services/API/api.service';
import {singleUserRequest,singleDateResponse } from '../DashBoard/Model/DashBoardModel';
import { Router } from '@angular/router';
declare var $;
@Component({
  selector: 'app-daywise-report',
  templateUrl: './daywise-report.component.html',
  styleUrls: ['./daywise-report.component.css']
})

export class DaywiseReportComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  dataTable: any;
  tableData = [];
  singleUserRequest:singleUserRequest;
  singleDateResponse:singleDateResponse[];

  date: string;
     constructor( @Inject(POPOUT_MODAL_DATA) public data: PopoutData,public API: ApiService, private router: Router) {
      this.singleDateResponse=[];
      this.singleUserRequest=new singleUserRequest();
      this.singleUserRequest.ToDate=data.reportdate;
      this.getdata();
     }

  ngOnInit(): void {
    this.date=this.data.reportdate;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 30,
      processing: true,
      dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print','pdf'
        ]
    };
  }
getdata(){
  this.API.PostData('/POS/getsingleDateData', this.singleUserRequest).subscribe(c => {
    if (c != null) {
      this.singleDateResponse=[];
      this.singleDateResponse=c;
      this.dtTrigger.next();
    }
  }),  (err) => {
    console.log('-----> err', err);
    this.router.navigateByUrl('/login');
  }
}
rerender(): void {
  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    // Destroy the table first
    dtInstance.destroy();
    // Call the dtTrigger to rerender again
    this.dtTrigger.next();
  });
 }
}
