export class hourlyRequest {
    mFromDate:string;
    mToDate:string;
    selectedLocations: selectedLocations[];
    constructor() {
        this.selectedLocations = [];
    }

}
export class selectedLocations {
    locationID: number;
    locationName: string;
}
export class hourlyResponse {
    TimeSlot:string;
    Amt:number;
}