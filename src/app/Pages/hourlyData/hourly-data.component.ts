import { Component, OnInit,ViewChildren,QueryList } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {hourlyRequest,hourlyResponse} from './Model/hourlyModel'
import { ApiService } from '../../Services/API/api.service';
import { GvarService } from '../../Services/Globel/gvar.service'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DataTableDirective } from 'angular-datatables';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label, Color ,SingleDataSet} from 'ng2-charts';
import { requestModel, userwiseResponse,serverStatus,waiters,selectedItems,selectedLocations } from '../../Pages/DashBoard/Model/DashBoardModel';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
@Component({
  selector: 'app-hourly-data',
  templateUrl: './hourly-data.component.html',
  styleUrls: ['./hourly-data.component.css']
})
export class HourlyDataComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings = { 
    singleSelection: false,
    idField: 'locationID',
    textField: 'locationName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 8,
    allowSearchFilter: true
  };


  selectedLocations:selectedLocations;
  objselectedItems:selectedItems;
  grandTotal:number=0;
  colors = [{ status: "Passed", color: "red" }, { status: "Approuved", color: "red" }, 
                { status: "warning", color: "green" }, { status: "Ignored", color: "yellow" }]
  showBarChart: boolean = false;
  selectedReport:string="";
  selectedData:string;
  validForm: boolean = false;
  showpieChart:boolean=false;
  disableDate:boolean=true;
  complaintCount=[];
  chartSelectedValue: string = "bar";
  // PIE Chart 
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: {
        //fontFamily: '"Arvo", serif',
        fontSize: 20,
      }
    }
  };
  public pieChartLabels: Label[];
  public pieChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];


  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // En PieChart
  // Bar chart Start
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return "Rs " + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function (c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
          });
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {}
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          stepSize: 500,
          // Return an empty string to draw the tick line but hide the tick label
          // Return `null` or `undefined` to hide the tick line entirely
        }
      }]
    }
  };
  public barChartLabels: Label[];
  public barChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsNaturewise: Label[];
  public barChartDataNaturewise: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsTrending: Label[];
  public barChartDataTrending: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];









  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;

  dtOptions0: any = {};
  dtTrigger0: Subject<any> = new Subject();

  hourlyResponse:hourlyResponse[];
  hourlyRequest:hourlyRequest;
  dateFormat:string="dd-MMMM-yyyy";
  hourlyFrom: FormGroup;
  valueFrom: Date;
  valueTo: Date;
  constructor(private router: Router,public API: ApiService, public GV: GvarService) { 
    this.hourlyRequest=new hourlyRequest();
    this.hourlyResponse=[];

    this.selectedLocations=new selectedLocations();
    this.objselectedItems=new selectedItems();


  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getLocations();
    this.objselectedItems.isActive=true;
    this.objselectedItems.isDeleted=false;
    this.objselectedItems.locationID=1;
    this.objselectedItems.locationName='CK-F6'
    this.selectedItems.push(this.objselectedItems);
   this.selectedLocations.locationID=1;
   this.selectedLocations.locationName='CK-F6';
    this.hourlyRequest.selectedLocations.push(this.selectedLocations);
    this.hourlyFrom.controls.dateFrom.setValue(new Date());
    this.hourlyFrom.controls.dateTo.setValue(new Date());
    this.selectedData='DD';


  }
  InitializeForm(): any {
    this.hourlyFrom = new FormGroup({
      dateFrom: new FormControl(this.valueFrom, [Validators.required]),
      dateTo: new FormControl(this.valueTo, [Validators.required]),
    });
  }
  getData(){
    this.validations();
    if (this.validForm == true) {
    var dateFrom=this.hourlyFrom.controls.dateFrom.value;
    var dateTo=this.hourlyFrom.controls.dateTo.value;
    this.hourlyRequest.mFromDate=dateFrom.toDateString();
    this.hourlyRequest.mToDate=dateTo.toDateString();


    this.API.PostData('/POS/getHourlyData', this.hourlyRequest).subscribe(c => {
        this.hourlyResponse=c;
        this.dtTrigger0.next();
        this.showBarChart=true;
        this.getBarChartHorizental();
      }), error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.router.navigateByUrl('/login');
      }
      
  }
  }
  validations(){
    if (this.hourlyFrom.controls.dateFrom.value == "" || this.hourlyFrom.controls.dateFrom.value == null) {
      Swal.fire({
        text: "Please select from date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.hourlyFrom.controls.dateTo.value == "" || this.hourlyFrom.controls.dateTo.value == null) {
      Swal.fire({
        text: "Please select to date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    this.validForm=true;
  }
   destroyDT = (tableIndex, clearData): Promise<boolean> => {
    return new Promise((resolve) => {
      this.datatableElement.forEach((dtElement: DataTableDirective, index) => {

        if (index == tableIndex) {
          if (dtElement.dtInstance) {

            if (tableIndex == 0) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 1) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            } else if (tableIndex == 2) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 3) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 4) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }
            else if (tableIndex == 5) {
              dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                if (clearData) {
                  dtInstance.clear();
                }
                dtInstance.destroy();
                resolve(true);
              });

            }

          }
          else {
            resolve(true);
          }

        }
      });
    });
  };
  getBarChartHorizental() {
        this.grandTotal=0;
        this.hourlyResponse.forEach(e => {
          this.grandTotal=this.grandTotal+e.Amt;
        });
        this.complaintCount = this.hourlyResponse.map((item) => {
          return item.Amt;
        });
        this.barChartDataNaturewise = [{ data: this.complaintCount, backgroundColor: '#2196f3',hoverBackgroundColor:'#28196D',fill:false }];
        var complaintDept = [];
        complaintDept = this.hourlyResponse.map((item) => {
            return item.TimeSlot;
        });
        this.barChartLabelsNaturewise = complaintDept;
      }
      onItemSelect(item: any) {
        this.hourlyRequest.selectedLocations.push(item);
      }
      onSelectAll(items: any) {
        this.hourlyRequest.selectedLocations=items;
      }
      deSelectAll(items: any) {
        this.hourlyRequest.selectedLocations=[];
      }
      ondeSelect(items: any ){
        this.hourlyRequest.selectedLocations.splice(this.hourlyRequest.selectedLocations.findIndex(ele => ele.locationID == items.locationID), 1);
      }
      getLocations(){
        this.API.getdata('/Locations/getLocations').subscribe(data=>{
          if(data!=null){
            this.dropdownList=data;
          }
        })
      }
}
