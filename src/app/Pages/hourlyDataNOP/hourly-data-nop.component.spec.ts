import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HourlyDataNOPComponent } from './hourly-data-nop.component';

describe('HourlyDataNOPComponent', () => {
  let component: HourlyDataNOPComponent;
  let fixture: ComponentFixture<HourlyDataNOPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HourlyDataNOPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HourlyDataNOPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
