import { Component, OnInit,ViewChildren,QueryList } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {hourlyRequestNOP,hourlyResponseNOP} from './Model/hourlyModelNOP'
import { ApiService } from '../../Services/API/api.service';
import { GvarService } from '../../Services/Globel/gvar.service'
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label, Color ,SingleDataSet} from 'ng2-charts';
import { Router } from '@angular/router';


@Component({
  selector: 'app-hourly-data-nop',
  templateUrl: './hourly-data-nop.component.html',
  styleUrls: ['./hourly-data-nop.component.css']
})
export class HourlyDataNOPComponent implements OnInit {
  grandTotal:number=0;
  colors = [{ status: "Passed", color: "red" }, { status: "Approuved", color: "red" }, 
                { status: "warning", color: "green" }, { status: "Ignored", color: "yellow" }]
  showBarChart: boolean = false;
  selectedReport:string="";
  selectedData:string;
  validForm: boolean = false;
  showpieChart:boolean=false;
  disableDate:boolean=true;
  complaintCount=[];
  chartSelectedValue: string = "bar";
  // PIE Chart 
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: {
        //fontFamily: '"Arvo", serif',
        fontSize: 20,
      }
    }
  };
  public pieChartLabels: Label[];
  public pieChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];


  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // En PieChart
  // Bar chart Start
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
    },
    scales: {
      xAxes: [{
        ticks: {}
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          stepSize: 10,
        }
      }]
    }
  };
  public barChartLabels: Label[];
  public barChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsNaturewise: Label[];
  public barChartDataNaturewise: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsTrending: Label[];
  public barChartDataTrending: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];




  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;

  dtOptions0: any = {};
  dtTrigger0: Subject<any> = new Subject();

  hourlyResponseNOP:hourlyResponseNOP[];
  hourlyRequestNOP:hourlyRequestNOP;
  dateFormat:string="dd-MMMM-yyyy";
  hourlyFrom: FormGroup;
  valueFrom: Date;
  valueTo: Date;
  constructor(public API: ApiService, public GV: GvarService,private router: Router) { 
    this.hourlyRequestNOP=new hourlyRequestNOP();
    this.hourlyResponseNOP=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
  }
  InitializeForm(): any {
    this.hourlyFrom = new FormGroup({
      dateFrom: new FormControl(this.valueFrom, [Validators.required]),
      dateTo: new FormControl(this.valueTo, [Validators.required]),
    });
  }
  getData(){
    this.validations();
    if (this.validForm == true) {
    var dateFrom=this.hourlyFrom.controls.dateFrom.value;
    var dateTo=this.hourlyFrom.controls.dateTo.value;
    this.hourlyRequestNOP.mFromDate=dateFrom.toDateString();
    this.hourlyRequestNOP.mToDate=dateTo.toDateString();
    this.API.PostData('/POS/getHourlyDataNOP', this.hourlyRequestNOP).subscribe(c => {
        this.hourlyResponseNOP=c;
        this.dtTrigger0.next();
        this.showBarChart=true;
        this.getBarChartHorizental();
      }), error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.router.navigateByUrl('/login');
      }
      
  }

  }
  validations(){
    if (this.hourlyFrom.controls.dateFrom.value == "" || this.hourlyFrom.controls.dateFrom.value == null) {
      Swal.fire({
        text: "Please select from date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.hourlyFrom.controls.dateTo.value == "" || this.hourlyFrom.controls.dateTo.value == null) {
      Swal.fire({
        text: "Please select to date.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    this.validForm=true;
  }
  getBarChartHorizental() {
    this.grandTotal=0;
    this.hourlyResponseNOP.forEach(e => {
      this.grandTotal=this.grandTotal+e.NOP;
    });
    this.complaintCount = this.hourlyResponseNOP.map((item) => {
      return item.NOP;
    });
    this.barChartDataNaturewise = [{ data: this.complaintCount, backgroundColor: '#2196f3',hoverBackgroundColor:'#28196D',fill:false }];
    var complaintDept = [];
    complaintDept = this.hourlyResponseNOP.map((item) => {
        return item.TimeSlot;
    });
    this.barChartLabelsNaturewise = complaintDept;
  }
}
