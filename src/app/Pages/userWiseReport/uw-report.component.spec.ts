import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UwReportComponent } from './uw-report.component';

describe('UwReportComponent', () => {
  let component: UwReportComponent;
  let fixture: ComponentFixture<UwReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UwReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UwReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
