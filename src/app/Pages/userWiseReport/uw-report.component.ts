import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, NumberValueAccessor } from '@angular/forms';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label, Color ,SingleDataSet} from 'ng2-charts';
import { ApiService } from '../../Services/API/api.service';
import { GvarService } from '../../Services/Globel/gvar.service'
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { requestModel, userwiseResponse,serverStatus,waiters,selectedItems,selectedLocations } from '../../Pages/DashBoard/Model/DashBoardModel';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {Locations} from '../Login/Model/locations'

@Component({
  selector: 'app-uw-report',
  templateUrl: './uw-report.component.html',
  styleUrls: ['./uw-report.component.css']
})
export class UwReportComponent implements OnInit {
  selectedLocations:selectedLocations;
  objselectedItems:selectedItems;
  dateFormat:string="dd-MMMM-yyyy";
  userwiseResponse:userwiseResponse;
  grandTotal:number=0;
  colors = [{ status: "Passed", color: "red" }, { status: "Approuved", color: "red" }, 
                { status: "warning", color: "green" }, { status: "Ignored", color: "yellow" }]
  serverStatus:serverStatus[];
  waiters:waiters[];
  Locations:Locations[];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings = { 
    singleSelection: false,
    idField: 'locationID',
    textField: 'locationName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 8,
    allowSearchFilter: true
  };
  
  showBarChart: boolean = false;
  selectedReport:string="";
  selectedData:string;
  validForm: boolean = false;
  showpieChart:boolean=false;
  disableDate:boolean=true;
  complaintCount=[];
  chartSelectedValue: string = "bar";
  // PIE Chart 
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: {
        //fontFamily: '"Arvo", serif',
        fontSize: 20,
      }
    }
  };
  public pieChartLabels: Label[];
  public pieChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];


  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // En PieChart
  // Bar chart Start
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return "Rs " + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function (c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
          });
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {}
      }],
      yAxes: [{
        ticks: {
          beginAtZero: false,
          stepSize: 500000,
          // Return an empty string to draw the tick line but hide the tick label
          // Return `null` or `undefined` to hide the tick line entirely
          callback: function (value, index, values) {
            // Convert the number to a string and splite the string every 3 charaters from the end
            // value = value.toString();
            // values = value.split(/(?=(?:...)*$)/);
            // // Convert the array to a string and format the output
            // value = values.join(',');
            // return 'PKR - ' + value;
            var val=Number(value) / 1e6 + 'M';
            return val
            
          }
        }
      }]
    }
  };
  public barChartLabels: Label[];
  public barChartData: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsNaturewise: Label[];
  public barChartDataNaturewise: ChartDataSets[] = [
    { data: [12, 68, 6] }
  ];

  public barChartLabelsTrending: Label[];
  public barChartDataTrending: ChartDataSets[] = [
    { data: [0, 0, 0] }
  ];

  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];
  // End Bar chart
  DashBoardForm: FormGroup;
  valueFrom: Date;
  requestModel: requestModel;
  valueFromTime: Date = new Date();
  valueTo: Date;
  valueToTime: Date = new Date();
  fromDate = new Date();
  toDate = new Date();
  constructor(public API: ApiService, public GV: GvarService, private router: Router) {
    this.requestModel = new requestModel();
    this.selectedLocations=new selectedLocations();
    this.userwiseResponse = new userwiseResponse();
    this.Locations=[];
    this.serverStatus=[];
    this.waiters=[];
    this.selectedItems=[];
    this.selectedItems=[];
    this.objselectedItems=new selectedItems();


  }

  ngOnInit(): void {
    this.userwiseResponse = new userwiseResponse();
    this.InitializeForm();
    this.getLocations();
    this.objselectedItems.isActive=true;
    this.objselectedItems.isDeleted=false;
    this.objselectedItems.locationID=1;
    this.objselectedItems.locationName='CK-F6'
    this.selectedItems.push(this.objselectedItems);
   this.selectedLocations.locationID=1;
   this.selectedLocations.locationName='CK-F6';
    this.requestModel.selectedLocations.push(this.selectedLocations);
    this.getWaiters();
    this.DashBoardForm.controls['selectedWaiter'].setValue("ALL", {onlySelf: true});
    this.DashBoardForm.controls['pmtMode'].setValue("ALL", {onlySelf: true});
    this.DashBoardForm.controls.dateFrom.setValue(new Date());
    this.DashBoardForm.controls.dateTo.setValue(new Date());
    this.selectedData='DD';

  }
  InitializeForm(): any {
    this.DashBoardForm = new FormGroup({
      dateFrom: new FormControl(this.valueFrom, [Validators.required]),
      dateTo: new FormControl(this.valueTo, [Validators.required]),
      pmtMode: new FormControl('', [Validators.required]),
      selectedWaiter: new FormControl('', [Validators.required]),

    });
  }
  dateChangeEvent(args, callFrom) {
    if (args.value == null) {
      return;
    }
    if (callFrom == "FromDate") {

      this.fromDate = args.value;
    }
    if (callFrom == "ToDate") {
      this.toDate = args.value;
    }
  }
  changeLeaveTypeEvent(event) {

  }
  getData() {

    this.validations();
    if (this.validForm == true) {
      this.showBarChart = true;
      this.showpieChart=false;
      this.getBarChartHorizental();
    }
  }
  changeGrapth(event) {
    this.chartSelectedValue = event.target.value;
    if (this.chartSelectedValue == "bh") {
      this.barChartType = 'bar';
      this.showBarChart = true;
      this.showpieChart=false;
    }
    else if (this.chartSelectedValue == "lh") {
      this.showBarChart = true;
      this.showpieChart=false;
      this.barChartType = 'line';
    }
    else if (this.chartSelectedValue == "pi") {
      this.showBarChart = false;
      this.showpieChart=true;
      this.PieChartDataFetch();
      this.barChartType = 'pie';
    }
  }
  getBarChartHorizental() {
    //this.getYear();
    this.requestModel.FromDate = this.DashBoardForm.controls.dateFrom.value;
    this.requestModel.ToDate = this.DashBoardForm.controls.dateTo.value;
    this.requestModel.userid=this.DashBoardForm.controls.selectedWaiter.value;
    this.requestModel.ModeofPay=this.DashBoardForm.controls.pmtMode.value;
    this.requestModel.dataType = "";
    this.API.PostData('/POS/getCashierWiseData', this.requestModel).subscribe(c => {
      if (c != null) {
        this.userwiseResponse = c;
        this.grandTotal=0;
        this.userwiseResponse.userwiseDetail.forEach(e => {
          this.grandTotal=this.grandTotal+e.NetAmount;
        });
        this.complaintCount = this.userwiseResponse.userwiseDetail.map((item) => {
          return item.NetAmount;
        });
        this.barChartDataNaturewise = [{ data: this.complaintCount, backgroundColor: '#2196f3',hoverBackgroundColor:'#28196D',fill:false }];
        var complaintDept = [];
        complaintDept = this.userwiseResponse.userwiseDetail.map((item) => {
            return item.userid;
        });
        this.barChartLabelsNaturewise = complaintDept;
      }
    },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      })
  }
  PieChartDataFetch() {
        this.complaintCount = this.userwiseResponse.userwiseDetail.map((item) => {
          return item.NetAmount;
        });
        var complaintCount = [];
        this.pieChartData = [{ data: this.complaintCount}];
        var complaintDept = [];
        complaintDept = this.userwiseResponse.userwiseDetail.map((item) => {
            return item.userid;
        });
        this.pieChartLabels = complaintDept;
  }
  validations() {

    if (this.requestModel.selectedLocations === undefined || this.requestModel.selectedLocations.length == 0) {
      Swal.fire({
        text: "Please select location.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    if (this.DashBoardForm.controls.selectedWaiter.value === undefined || this.DashBoardForm.controls.selectedWaiter.value == "") {
      Swal.fire({
        text: "Please select waiter.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
    
    if (this.DashBoardForm.controls.pmtMode.value === undefined || this.DashBoardForm.controls.pmtMode.value == "") {
      Swal.fire({
        text: "Please select payment mode.",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.validForm = false;
      return;
    }
      if (this.DashBoardForm.controls.dateFrom.value == "" || this.DashBoardForm.controls.dateFrom.value == null) {
        Swal.fire({
          text: "Please select from date.",
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.validForm = false;
        return;
      }
      if (this.DashBoardForm.controls.dateTo.value == "" || this.DashBoardForm.controls.dateTo.value == null) {
        Swal.fire({
          text: "Please select to date.",
          icon: 'error',
          confirmButtonText: 'OK'
        });
        this.validForm = false;
        return;
      }
    this.validForm = true;
  }
  onItemSelect(item: any) {
    this.requestModel.selectedLocations.push(item);
    this.getWaiters();
  }
  onSelectAll(items: any) {
    this.requestModel.selectedLocations=items;
    this.getWaiters();
  }
  deSelectAll(items: any) {
    this.requestModel.selectedLocations=[];
    this.getWaiters();
  }
  ondeSelect(items: any ){
    this.requestModel.selectedLocations.splice(this.requestModel.selectedLocations.findIndex(ele => ele.locationID == items.locationID), 1);
    this.getWaiters();
  }
  getLocations(){
    this.API.getdata('/Locations/getLocations').subscribe(data=>{
      if(data!=null){
        this.dropdownList=data;
      }
    })
  }
  getTheColor(status) {
    return this.colors.filter(item => item.status === status)[0].color 
    // could be better written, but you get the idea
}
getWaiters(){
  this.API.PostData('/POS/getWaiters', this.requestModel).subscribe(c => {
    if (c != null) {
      this.waiters=c;
    }
  });
  }
}
