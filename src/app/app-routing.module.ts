import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Pages/Login/login.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component'
import { Auth } from './Services/Guard/guard.service'
import { ComplaintComponent } from './Pages/Complaints/complaint.component';
import { DashBoardComponent } from './Pages/DashBoard/dash-board.component';
import { AdminAreaComponent } from './Pages/AdminArea/admin-area.component';
import { RasLocationsComponent } from './Pages/AdminArea/RasLocations/ras-locations.component'
import { DepartmentsComponent } from './Pages/AdminArea/Departments/departments.component'
import { DashBoard2Component } from './Pages/DashBoard-2/dash-board2/dash-board2.component'
import { CwDashboardComponent } from './Pages/CWDashboard/cw-dashboard.component'
import { UwReportComponent } from './Pages/userWiseReport/uw-report.component'
import { ShipperComponent } from './Pages/AdminArea/Shipper/shipper.component'
import { HourlyDataNOPComponent } from './Pages/hourlyDataNOP/hourly-data-nop.component'
import { FoodwiseReportComponent } from './Pages/FoodWiseReport/foodwise-report.component'
import { AgentTypeComponent } from './Pages/AdminArea/AgentType/agent-type.component'
import { HourlyDataComponent } from './Pages/hourlyData/hourly-data.component'
import { ConsigneeComponent } from './Pages/AdminArea/Consignee/consignee.component'
import { AgentsComponent } from './Pages/AdminArea/Agents/agents.component'
import { UdmMasterComponent } from './Pages/AdminArea/UDMMaster/udm-master.component'
import { CommodityComponent } from './Pages/AdminArea/Commodity/commodity.component'
import { VehiclesComponent } from './Pages/AdminArea/Vehicles/vehicles.component'
import { ForwaderComponent } from './Pages/AdminArea/Forwader/forwader.component'
import { NatureOfGoodsComponent } from './Pages/AdminArea/NatureofGoods/nature-of-goods.component'
import { AirLinesComponent } from './Pages/AdminArea/AirLines/air-lines.component'
import { ExportComponent } from './Pages/Export/export.component'
import { AcceptanceComponent } from './Pages/Export/Acceptance/acceptance.component'
import { FlightsComponent } from './Pages/Export/Flights/flights.component'
import { InquiryComponent } from './Pages/Export/Inquiry/inquiry.component'
import { NoticeTypesComponent } from './Pages/AdminArea/NoticeTypes/notice-types.component'
import { ExaminationComponent } from './Pages/Export/Examination/examination.component'
import { ScanningComponent } from './Pages/Export/Scanning/scanning.component'
import { ULDTypesComponent } from './Pages/AdminArea/ULDTypes/uldtypes.component'
import {ULDRouteComponent} from './Pages/ULD/uldroute.component'
import {ULDComponent} from './Pages/ULD/ULD/uld.component'
const routes: Routes = [

  {
    path: '', component: LayoutComponent, canActivate: [Auth], children: [
      { path: 'Complaint', component: ComplaintComponent, canActivate: [Auth] },
      { path: 'Dashboard', component: DashBoardComponent },
      { path: 'Locations', component: RasLocationsComponent },
      { path: 'Departments', component: DepartmentsComponent },
      { path: 'DashBoard2', component: DashBoard2Component },
      { path: 'CWDashBoard', component: CwDashboardComponent },
      { path: 'userWiseReport', component: UwReportComponent },
      { path: 'timeslotwise', component: HourlyDataComponent },
      { path: 'timeslotwiseNOP', component: HourlyDataNOPComponent },
      { path: 'foodwiseReport', component: FoodwiseReportComponent },
      {
        path: 'Admin', component: AdminAreaComponent, children: [
          { path: 'Shipper', component: ShipperComponent },
          { path: 'Consignee', component: ConsigneeComponent },
          { path: 'AgentType', component: AgentTypeComponent },
          { path: 'Agents', component: AgentsComponent },
          { path: 'UDM Master', component: UdmMasterComponent },
          { path: 'Commidity', component: CommodityComponent },
          { path: 'Vehicles', component: VehiclesComponent },
          { path: 'Forwader', component: ForwaderComponent },
          { path: 'NatureOfGoods', component: NatureOfGoodsComponent },
          { path: 'AirLines', component: AirLinesComponent },
          { path: 'NoticeType', component: NoticeTypesComponent },
          { path: 'ULDTypes', component: ULDTypesComponent },
        ]
      },
      {
        path: 'Export', component: ExportComponent, children: [
          { path: 'Acceptance', component: AcceptanceComponent },
          { path: 'Flights', component: FlightsComponent },
          { path: 'Inquiry', component: InquiryComponent },
          { path: 'Examination', component: ExaminationComponent },
          { path: 'Scanning', component: ScanningComponent },


        ]
      },
      {
        path: 'ULD', component: ULDRouteComponent, children: [
          { path: 'ULD', component: ULDComponent },

        ]
      },
    ]
  },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
