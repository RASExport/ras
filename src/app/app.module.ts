import { NgModule, Pipe } from '@angular/core';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { ApiService } from './Services/API/api.service'
import { Auth } from './Services/Guard/guard.service'
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { ChartsModule } from 'ng2-charts';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { ListBoxModule } from '@syncfusion/ej2-angular-dropdowns';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BnNgIdleService } from 'bn-ng-idle';
import { PortalModule } from '@angular/cdk/portal';
import { PopoutService } from './Pages/Shared/Service/popout.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NumberOnlyDirective } from './number-only.directive';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { IgxGridModule, IgxMaskModule, IgxComboModule, IgxDropDownModule, IgxSelectModule, IgxExpansionPanelModule, IgxCheckboxModule, IgxIconModule, IgxInputGroupModule, IgxButtonModule, IgxRippleModule, IgxDatePickerModule, IgxTimePickerModule } from "igniteui-angular";

// Material UI Modules
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import ResizeObserver from 'resize-observer-polyfill'


import { LoginComponent } from './Pages/Login/login.component';
import { DashBoardComponent } from './Pages/DashBoard/dash-board.component';
import { ComplaintComponent } from './Pages/Complaints/complaint.component';
import { LeftmenuComponent } from './Pages/Shared/LeftMenu/leftmenu.component';
import { TopbarComponent } from './Pages/Shared/TopBar/topbar.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';
import { AdminAreaComponent } from './Pages/AdminArea/admin-area.component';
import { RasLocationsComponent } from './Pages/AdminArea/RasLocations/ras-locations.component';
import { DepartmentsComponent } from './Pages/AdminArea/Departments/departments.component';
import { DashBoard2Component } from './Pages/DashBoard-2/dash-board2/dash-board2.component';
import { CwDashboardComponent } from './Pages/CWDashboard/cw-dashboard.component';
import { UwReportComponent } from './Pages/userWiseReport/uw-report.component';
import { DaywiseReportComponent } from './Pages/dayWiseReport/daywise-report.component';
import { HourlyDataComponent } from './Pages/hourlyData/hourly-data.component';
import { HourlyDataNOPComponent } from './Pages/hourlyDataNOP/hourly-data-nop.component';
import { FoodwiseReportComponent } from './Pages/FoodWiseReport/foodwise-report.component';
import { ShipperComponent } from './Pages/AdminArea/Shipper/shipper.component';
import { ConsigneeComponent } from './Pages/AdminArea/Consignee/consignee.component';
import { AgentTypeComponent } from './Pages/AdminArea/AgentType/agent-type.component';
import { AgentsComponent } from './Pages/AdminArea/Agents/agents.component';
import { UdmMasterComponent } from './Pages/AdminArea/UDMMaster/udm-master.component';
import { CommodityComponent } from './Pages/AdminArea/Commodity/commodity.component';
import { VehiclesComponent } from './Pages/AdminArea/Vehicles/vehicles.component';
import { ForwaderComponent } from './Pages/AdminArea/Forwader/forwader.component';
import { NatureOfGoodsComponent } from './Pages/AdminArea/NatureofGoods/nature-of-goods.component';
import { AirLinesComponent } from './Pages/AdminArea/AirLines/air-lines.component';
import { AcceptanceComponent } from './Pages/Export/Acceptance/acceptance.component';
import { ExportComponent } from './Pages/Export/export.component';
import { FlightsComponent } from './Pages/Export/Flights/flights.component';
import { TwoDigitDecimaNumberDirective } from './Pages/Directive/two-digit-decima-number.directive';
import { InquiryComponent } from './Pages/Export/Inquiry/inquiry.component';
import { NoticeTypesComponent } from './Pages/AdminArea/NoticeTypes/notice-types.component';
import { ExaminationComponent } from './Pages/Export/Examination/examination.component';
import { ScanningComponent } from './Pages/Export/Scanning/scanning.component';
import { ULDTypesComponent } from './Pages/AdminArea/ULDTypes/uldtypes.component';
import { ULDComponent } from './Pages/ULD/ULD/uld.component';
import { ULDRouteComponent } from './Pages/ULD/uldroute.component';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashBoardComponent,
    ComplaintComponent,
    LeftmenuComponent,
    TopbarComponent,
    LayoutComponent,
    AdminAreaComponent,
    RasLocationsComponent,
    DepartmentsComponent,
    DashBoard2Component,
    CwDashboardComponent,
    UwReportComponent,
    DaywiseReportComponent,
    HourlyDataComponent,
    HourlyDataNOPComponent,
    FoodwiseReportComponent,
    ShipperComponent,
    ConsigneeComponent,
    AgentTypeComponent,
    AgentsComponent,
    UdmMasterComponent,
    CommodityComponent,
    VehiclesComponent,
    ForwaderComponent,
    NatureOfGoodsComponent,
    AirLinesComponent,
    AcceptanceComponent,
    ExportComponent,
    FlightsComponent,
    NumberOnlyDirective,
    TwoDigitDecimaNumberDirective,
    InquiryComponent,
    NoticeTypesComponent,
    ExaminationComponent,
    ScanningComponent,
    ULDTypesComponent,
    ULDComponent,
    ULDRouteComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    DataTablesModule,
    NgxMaskModule.forRoot(),
    DatePickerModule,
    ChartsModule,
    ListBoxModule,
    PortalModule,
    IgxDatePickerModule,
    HammerModule,
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    IgxIconModule,
    IgxInputGroupModule,
    IgxButtonModule,
    IgxRippleModule,
    IgxTimePickerModule,
    TimepickerModule.forRoot(),
    IgxCheckboxModule, IgxGridModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    IgxExpansionPanelModule, IgxComboModule, IgxDropDownModule, IgxSelectModule, IgxMaskModule
  ],
  providers: [ApiService, Auth, BnNgIdleService, { provide: LocationStrategy, useClass: HashLocationStrategy }, Pipe, PopoutService],
  bootstrap: [AppComponent]
})
export class AppModule { }
